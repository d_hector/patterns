package com.example;


class TruckObserver implements Observer {

    @Override
    public void update(int engine) {
        if (engine==Observable.TRUCK){
            System.out.println("Truck setup");
        }
    }
}
