package com.example;


interface Observer {
    void update(int engine);
}
