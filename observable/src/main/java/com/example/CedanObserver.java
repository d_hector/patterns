package com.example;


class CedanObserver implements Observer {

    @Override
    public void update(int engine) {
        if (engine==Observable.CEDAN){
            System.out.println("Cedan setup");
        }
    }
}
