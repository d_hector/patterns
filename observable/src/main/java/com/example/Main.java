package com.example;


public class Main {

    private final VehicleObservable observable=new VehicleObservable();

    Main(){
        Observer truck=new TruckObserver();
        Observer cedan=new CedanObserver();

        observable.registerObserver(truck);
        observable.registerObserver(cedan);

        observable.setEngine(Observable.TRUCK);
        observable.setEngine(Observable.CEDAN);

        observable.removeObserver(truck);
        observable.removeObserver(cedan);
    }

    public static void main(String arg[]){
        new Main();
    }
}
