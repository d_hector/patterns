package com.example;


import java.util.ArrayList;

class VehicleObservable implements Observable {

    private final ArrayList<Observer> observers = new ArrayList<Observer>();
    private int engine;

    @Override
    public void setEngine(int engine) {
        this.engine = engine;
        notifyObservers();
    }

    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        System.out.println("Notifying Observers on change in Loan interest rate");
        for (Observer ob : observers) {
            ob.update(engine);
        }
    }
}
