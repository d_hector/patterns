package com.example;


interface Observable {

    int TRUCK = 0;
    int CEDAN = 1;

    void setEngine(int truck);

    void registerObserver(Observer observer);

    void removeObserver(Observer observer);

    void notifyObservers();
}
