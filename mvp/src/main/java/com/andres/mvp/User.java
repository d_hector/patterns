package com.andres.mvp;


interface User {
    String getName();
    String getLastName();
    int getId();
}
