package com.andres.mvp;


import java.util.Collection;

class UserPresenter implements Presenter {

    private final Model model;
    private View view;

    UserPresenter(View view) {
        this.view = view;
        model = new UserModel(modelCallback);
    }

    @Override
    public void onViewCreated() {
        model.loadUsers();
    }

    @Override
    public void onViewResume() {
        model.getUsers();
    }

    private final Model.ModelCallback modelCallback = new Model.ModelCallback() {
        @Override
        public void onUsersLoaded(Collection<User> users) {
            view.showUsers(users);
        }
    };
}
