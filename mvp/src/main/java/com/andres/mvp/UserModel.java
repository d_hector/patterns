package com.andres.mvp;


import java.util.ArrayList;
import java.util.Collections;

class UserModel implements Model {

    private static final int MAX_USERS = 20;
    private ModelCallback callback;
    private final ArrayList<User> users=new ArrayList<>();

    UserModel(ModelCallback callback){
        this.callback = callback;
    }

    @Override
    public void getUsers() {
        callback.onUsersLoaded(Collections.unmodifiableCollection(users));
    }

    @Override
    public void loadUsers() {
        for (int i = 0; i < MAX_USERS; i++) {
            final int finalI = i;
            users.add(new User() {
                @Override
                public String getName() {
                    return "Username "+ finalI;
                }

                @Override
                public String getLastName() {
                    return "User last name "+finalI;
                }

                @Override
                public int getId() {
                    return finalI;
                }
            });
        }
    }
}
