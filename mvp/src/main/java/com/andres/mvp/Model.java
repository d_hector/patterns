package com.andres.mvp;


import java.util.Collection;

interface Model {
    void getUsers();

    void loadUsers();

    interface ModelCallback{
        void onUsersLoaded(Collection<User> users);
    }
}
