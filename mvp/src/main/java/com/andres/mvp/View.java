package com.andres.mvp;


import java.util.Collection;

public interface View {
    void showUsers(Collection<User> users);
}
