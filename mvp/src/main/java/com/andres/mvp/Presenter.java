package com.andres.mvp;


interface Presenter {

    void onViewCreated();

    void onViewResume();
}
