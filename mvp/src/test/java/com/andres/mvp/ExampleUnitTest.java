package com.andres.mvp;

import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void UserLoadRun() throws Exception {
        Model user =new UserModel(new Model.ModelCallback() {
            @Override
            public void onUsersLoaded(Collection<User> users) {
                for (User user : users) {
                    System.out.println(user.getName());
                }
            }
        });
        user.loadUsers();
        user.getUsers();
    }
}